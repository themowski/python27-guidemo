#! /bin/python2
"""Sample Tkinter GUI shell

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.
"""

import Tkinter as tk


class GuiDemo(tk.Frame):
    """Sample GUI, based on a tk.Frame object."""
    
    def __init__(self, master=None):
        """Initialize a GuiDemo instance."""
        tk.Frame.__init__(self, master, width=250, height=100)
        self.grid()


# ----------------------------------------------#
#   Main                                        #
# ----------------------------------------------#
if __name__ == "__main__":
    # Set an icon
    try:
        tk.Tk().iconbitmap("pycon.ico")
    except:
        pass

    # Initialize the view
    view = GuiDemo()

    # Configure window properties
    view.master.resizable(width=False, height=False)
    view.master.title("GuiDemo")
    
    # Run the application
    view.mainloop()
